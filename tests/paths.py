from pathlib import Path

PROJECT_BASE = Path(__file__).parent.parent
TEST_BASE = Path(__file__).parent
TEST_RESOURCES_BASE = TEST_BASE.joinpath('resources')
TEST_RES_DECL = TEST_RESOURCES_BASE / 'declarative'
