#! /usr/bin/env python3

__author__ = "Peter Stanko"
__copyright__ = "Copyright 2016, The Project Name"
__credits__ = ["Peter Stanko"]
__license__ = "BSD"
__version__ = "0.0.1-SNAPSHOT"
__maintainer__ = "Peter Stanko"
__email__ = "wermington@gmail.com"
__status__ = "Production"
