import ktdk.KTDK
import ktdk.flavours.CMake
import ktdk.flavours.cpp
import ktdk.flavours.doxygen
import ktdk.fs
import ktdk.general
import ktdk.processors.Diff
import ktdk.testing


"""
Process
"""
process_valgrind = processor.Valgrind

process_diff = processors.Diff
process_diff.stdout.assert_string('.....')
process_diff.stderr.assert_file('.....')
process.diff.exit_code.assert_greater(10)
process_diff.points = 10
process_diff.chain(process_valgrind)

"""
Executable
"""
executable = cpp.Executable('hello')
executable.stdin(...)

"""
Valgrind test definition
"""
hello_test = cpp.ValgrindTest('Hello test')

hello_test.file_actions.register(None)
hello_test.compilation.register(None)
hello_test.exec.register(executable)

hello_test.executable.stdin(StringIO("......"))
hello_test.executable.params.append("--version...")
hello_test.executable.timeout(1000)
hello_test.valgrind.params("....")

hello_test.cached = False  # will require to run each time
hello_test.name = "....."
hello_test.desc = "Test of the hello world"
hello_test.processors.register(process_diff)

doxygen = flavours.doxygen.Test()
doxygen.points = 10

"""
File manipulation
"""
fs = ktdk.fs.FileActions()
fs.project.files(frm='tests/tests_*.cpp', to='tests/')
fs.project.dir(frm='res', to='res')
fs.submssion.dir(frm='src', to='src')
fs.project.files(frm='CMakeLists.txt')

"""
Compilation
"""
cmake = Cmake.Compilation()
cmake.targets.add_label('hello', path='bin/hello')

"""
Scenario nanecisto
"""
scenario = CMake.Scenario('Cmake nanecisto')
scenario.tags.add('nanecisto')
scenario.layout.project(CMake.Project)  # default
scenario.layout.submission(general.Submission)  # default
scenario.layout.workspace(CMake.Workspace)  # default

scenario.fs.register(fs)
scenario.compilation.register(cmake)
scenario.tests.register(hello_test)
scenario.tests.register(doxygen)

bin = Cpp.Scenario('Raw compilation')
bin.tags.add('naostro')
bin.layout.project(CMake.Project)
bin.compilation.register(compilation)

compilation = Cpp.Compilation(compiler="g++", flags=["-Wall", "-Wextra", "-pedantic"])
compilation.flags.add('-g')
target = Cpp.Target('naostro', executable="naostro_bin")
target.sources("src/naostro_*.cpp")
compilation.targets.register(target)
compilation.targets.add_target(target)

ktdk = KTDK.get_instance()
ktdk.register(scenario)
ktdk.register(bin)

"""
 Phaseses are grouped in Scenario.
 Scenarios can be partial.
 New scenario can be created from another scenario and continue:
 Scenario is High-Level Wrapper

 Scenario.CMake phases:
 * FileActions - each action cached (when already executed do not execute again unless required)
 * Compilation - each compilation cached - same as above
 * Binary Manipulation - cached
 * Execution and testing
 * Process results
"""
