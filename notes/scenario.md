# Scenario

## Components:
* Phases
* Layouts
* name, description, tags

## Classes:

# Task
* execute
* enable/disable
* status

### Tasks
* container: List<Task>
* register

### Phase:
* name, description
* requirements
* tasks: Tasks
* execute
* disable/enable
* state

### Phases
* container: List<Phase>
* register - registers phase
* delete - removes phase
* execute
* stop_phase - Stop at this phase - do not continue
