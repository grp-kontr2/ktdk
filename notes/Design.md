# Desing

## Structure
```
/ktdk
- /download
    * git
    * svn
    * wget
    * file_location
- /fs
    * move, copy, delete, merge dirs, filter
- /bin_manip
    * rename_symbol
- /compile
    * cmake, make
    * mvn, gradle
    * gcc, g++, clang
    * pip
    * rake
- /exec
    * prepares executable,
    * exec test (mvn, rake, pip)
- /process
    * executes executable
    * process output
    * uses processor - DIFF, Google TEST, JUNIT ....
    * add points (optional)
- /report
    * reporter - json
    * report from each step (compilation....)
    * file reporter, rest reporter, websocket reporter
- /tools
    * compress
    * logging
    * diff
    * itnernal
```
----
## REPORT 
Contains:
* Target (Nanecisto, nastro)
* Step
    * Download - 404
    * FS - file missing
    * bin_manip - symbol missing
    * exec - timeout, exit code
    * process - diff, test failed
* visibility (role - teacher, student)
* message
* Test name (Should print hello world)
* type: internal - caused by internal error (not students)
----

## EXEC - Preparation
```
exec = Executable(compilation_resut.binaries['test01'])
exec.stdin(FILE|"ahoj_svet")
exec.assert_stdout(FILE|STRING) // not required
exec.assert_stderr(FILE|STRING|EMPTY)
exec.assert_exit(0)
exec.timeout(1000) // or will be used default
```
- will use builder pattern
- prepares executable io
----
## PROCESS
```
process = Process(executable, reporter_type)
process.processor(JUNIT|CATCH|DIFF)
process_result = process.process()
```
- will use builder pattern
- will execute binary
- parse output and report result
----
## Reporting
- each phase
- aggregation of messages

## KTDK config

```python3
config = {
    'project_source': 'git|...',
    'submission_source': 'git|...',
    'base_dir': '/tmp',
    'target': 'naostro','
}
ktdk = KTDK(config)

```



