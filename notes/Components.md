# Design
## Components
* Scenario
* KTDK
* layout
* layout.Project : DirStructure
* layout.Submission: DirStructure
* layout.Workspace: DirStructure
* fs.Files
* phases.Compilation
* phases.Executable
* testing
* testing.Test
* testing.TestExecution

---
## Classes

### Scenario:
* log: Logger
* report: Repoter
* tags: Tags
* name: string
* desc: string

* fs
* compilation
* tests
* process
---

### Layout
* project: DirStructure
* submission: DirStructure
* workspace: DirStructure
* custom: Dictionary<DirStructure>

### Layout.Project:
* scripts: fs.Scripts
* resources: fs.Resources
* sources: fs.Sources

### Layout.Submssion:
* sources: fs.Sources
* resources: fs.Resources

### Layout.Workspace:
* sources: fs.Sources
* resouces: fs.Resources

### fs.FilesManager
* from: path
* to: path
* files
* dir
* ...

### fs.Files
* project: FilesManager
* submission: FilesManager
* workspace: FilesManager

### Compilation:
* targets: Targets
* status: Status
* executables: Executables

### Targets:
* targets: Dict<Targets>
* add_label
* add_target:: Target ->

### Target:
* sources: Array<Files>
* cc: CompilerContext
* name: String
* output: String

### Executables:
* executables: Array<Executable>

### Executable:
* path: Path




